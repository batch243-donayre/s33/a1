/*	
	3. Create a fetch request using the GET method that will retrieve all 
	   the to do list items from JSON Placeholder API.
	4. Using the data retrieved, create an array using the map method to return 
	   just the title of every item and print the result in the console.
*/

	const arrayList = [];
    fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {

        data.map((elem) => {
            arrayList.push(elem.title);
        });
    });
    console.log(arrayList);
	
/*
	5. Create a fetch request using the GET method that will retrieve a single to do list 
		item from JSON Placeholder API.
	6. Using the data retrieved, print a message in the console that will provide 
		the title and status of the to do list item.
*/

	fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((json) => console.log(json))


/*
	7. Create a fetch request using the POST method that will create a 
	to do list item using the JSON Placeholder API.
*/

	fetch('https://jsonplaceholder.typicode.com/todos',{

		method : 'POST',
		headers :{
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			id: 201,
			title: 'Created To Do List Item',
			completed: false
		}),

	})

	.then((response) => response.json())
	.then((json) => console.log(json));
	
/*

	8. Create a fetch request using the PUT method that will update a to do
		list item using the JSON Placeholder API.
	9. Update a to do list item by changing the data structure to contain
		the following properties:
		a. Title
		b. Description
		c. Status
		d. Date Completed
		e. User ID

*/
	fetch('https://jsonplaceholder.typicode.com/todos/1',{

		method: 'PUT',
		headers:{
			'Content-type' : 'application/json',
		},
		body : JSON.stringify({
			userId: 1,
			id: 1,
			status: 'Pending',
			title: 'Updated To Do List Item',
			dateCompleted: 'Pending'
		})
	})

	.then((response) => response.json())
	.then((json) => console.log(json));



/*
	10. Create a fetch request using the PATCH method that will update a to
		do list item using the JSON Placeholder API.
	11. Update a to do list item by changing the status to complete and add
		a date when the status was changed.
	
*/

		fetch('https://jsonplaceholder.typicode.com/todos/1',{

			method: 'PATCH',
			headers:{
				'Content-type' : 'application/json',
			},
			body: JSON.stringify({
				status: 'Complete',
				dateCompleted: '11/29/22'
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));



/*
	12. Create a fetch request using the DELETE method that will delete an
		item using the JSON Placeholder API.
*/

		fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method : "DELETE"
		})